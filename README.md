# README #

Segue algumas considerações sobre o código.


* Todas as técnologias exigidas foram aplicadas.

* No caso do retorno do json do usuário, resolvi seguir a dica do desafio de não enviar o id sequencial do banco. Neste caso, o json de retorno terá uma propriedade "code" que será a chave usada no GET do perfil do usuário.

* A chamada ao perfil do usuário, como já mencionado, é feito por um GET passando no path param o "code". **Além disso deve ser passado um header Authorization com um token Bearer que é retornado junto com o login ou cadastro.**

##Rodando a aplicação##

O processo de build é feito pelo gradle como solicitado. Utilizei a gretty como embedded server.

Para buildar é só rodar **gradle appStart**

Os endpoints são:

* Create User, POST, /concrete-challenge/api/user.
* Profile, GET, /concrete-challenge/api/user/{code}.
* Login, POST, /concrete-challenge/api/login.