package com.concrete.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.concrete.controller.UserController;
import com.concrete.entity.Phone;
import com.concrete.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/applicationContext.xml")
@FixMethodOrder(MethodSorters.JVM)
public class UserControllerTest {
	
	@Autowired
	UserController userController;
	
	@Test
	public void createUserTest(){
		User user = new User();
		user.setName("Jo�o da Silva");
		user.setEmail("joao@silva.org");
		user.setPassword("hunter2");
		Phone phone = new Phone();
		phone.setDdd(21);
		phone.setNumber(987654321);
		
		user.setPhones(new ArrayList<Phone>());
		user.getPhones().add(phone);
		
		user = (User) userController.create(user).getEntity();
		
		assertNotNull(user.getId());
		assertEquals(user.getId().longValue(), 0L);
		assertEquals(user.getPassword(), "f3bbbd66a63d4bf1747940578ec3d0103530e21d");
		
	}
	

}
