package com.concrete.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.concrete.controller.LoginController;
import com.concrete.controller.UserController;
import com.concrete.entity.Phone;
import com.concrete.entity.User;
import com.concrete.vo.LoginVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/applicationContext.xml")
@FixMethodOrder(MethodSorters.JVM)
public class LoginTest {
	
	@Autowired
	UserController userController;
	
	@Autowired
	LoginController loginController;
	
	@Test
	public void loginTest(){
		
		User user = new User();
		user.setName("Jo�o da Silva");
		user.setEmail("joao@silva.org");
		user.setPassword("hunter2");
		Phone phone = new Phone();
		phone.setDdd(21);
		phone.setNumber(987654321);
		
		user.setPhones(new ArrayList<Phone>());
		user.getPhones().add(phone);
		
		user = (User) userController.create(user).getEntity();
		
		User user2 = (User) loginController.login(new LoginVO("joao@silva.org","hunter2")).getEntity();
		
		assertNotNull(user.getId());
		assertEquals(user.getName(), user2.getName());
		assertEquals(user.getEmail(), user2.getEmail());
		assertEquals(user.getPassword(), user2.getPassword());
		
	}

}
