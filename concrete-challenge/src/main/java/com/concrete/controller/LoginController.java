package com.concrete.controller;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.concrete.service.LoginService;
import com.concrete.vo.ErrorMessageVO;
import com.concrete.vo.LoginVO;

@Path("/login")
@Component
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@PermitAll
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response login(LoginVO vo) {
		try {
			return Response.ok(loginService.login(vo)).build();
		} catch (NotAuthorizedException e) {
			return e.getResponse();
		} catch (Throwable t) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessageVO(t.getMessage())).build();
		}
	}
	
}
