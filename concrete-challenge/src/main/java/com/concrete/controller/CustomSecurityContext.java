package com.concrete.controller;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import lombok.AllArgsConstructor;

import com.concrete.entity.User;

@AllArgsConstructor
public class CustomSecurityContext implements SecurityContext {

	private User user;
	
	@Override
	public Principal getUserPrincipal() {
		return user;
	}

	@Override
	public boolean isUserInRole(String role) {
		return false;
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public String getAuthenticationScheme() {
		return null;
	}

}
