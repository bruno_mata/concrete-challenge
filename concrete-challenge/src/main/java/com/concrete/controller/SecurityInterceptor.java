package com.concrete.controller;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.PermitAll;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.concrete.exception.TokenExpiredException;
import com.concrete.service.SecurityService;
import com.concrete.vo.ErrorMessageVO;

@Provider
@Component
public class SecurityInterceptor implements ContainerRequestFilter {

	private static final String AUTHORIZATION = "Authorization";
	private static final String BEARER = "Bearer ";
	
	@Autowired
	private SecurityService securityService;
	
	private static final org.jboss.resteasy.core.Headers<Object> httpHeaders = new org.jboss.resteasy.core.Headers<Object>();
	
	static {
		httpHeaders.add("Content-type", MediaType.APPLICATION_JSON);
	}

	private static final ServerResponse ACCESS_DENIED 	 = new ServerResponse(new ErrorMessageVO("N�o autorizado").toJson(), 401, httpHeaders);
	private static final ServerResponse TOKEN_EXPIRED 	 = new ServerResponse(new ErrorMessageVO("Sess�o Inv�lida").toJson(), 401, httpHeaders);
	private static final ServerResponse SERVER_ERROR 	 = new ServerResponse(new ErrorMessageVO("Erro Interno").toJson(), 500,	httpHeaders);
	
	@Override
	public void filter(ContainerRequestContext ctx) throws IOException {
		
		try {
			ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) ctx.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
			Method method = methodInvoker.getMethod();
			
			if (!method.isAnnotationPresent(PermitAll.class)) {
				
				String accessToken = ctx.getHeaderString(AUTHORIZATION);
				
				if (StringUtils.isEmpty(accessToken)){
					ctx.abortWith(ACCESS_DENIED);
					return;
				}
				ctx.setSecurityContext(new CustomSecurityContext(securityService.checkToken(accessToken.replace(BEARER, ""))));
			}
			
		} catch (NotAuthorizedException e) {
			ctx.abortWith(e.getResponse());
		} catch (TokenExpiredException e) {
			ctx.abortWith(TOKEN_EXPIRED);
		} catch (Throwable e) {
			e.printStackTrace();
			ctx.abortWith(SERVER_ERROR);
		}
		
	}

}
