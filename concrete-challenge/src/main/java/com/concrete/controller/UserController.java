package com.concrete.controller;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.concrete.entity.User;
import com.concrete.exception.UserAlreadyExistsException;
import com.concrete.service.UserService;
import com.concrete.vo.ErrorMessageVO;

@Path("/user")
@Component
public class UserController {
	
	@Autowired
	private UserService userService;

	@PermitAll
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response create(User user) {
		try {
			return Response.ok().entity(userService.createUser(user)).build();
		} catch(UserAlreadyExistsException e) {
			return Response.status(Status.CONFLICT).entity(new ErrorMessageVO("E-mail j� existente")).build();
		} catch (Throwable t) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessageVO(t.getMessage())).build();
		}
    }
	
	@GET
	@Path("/{code}")
	@Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("code") String userCode, @Context SecurityContext context) {
		
		User user = (User) context.getUserPrincipal();
		
		if (!user.getCode().equals(userCode)) {
			return Response.status(Status.UNAUTHORIZED).entity(new ErrorMessageVO("N�o autorizado")).build();
		}
		
		return Response.ok().entity(user).build();
    }
	
}
