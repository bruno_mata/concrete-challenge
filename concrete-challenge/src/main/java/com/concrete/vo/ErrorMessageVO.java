package com.concrete.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ErrorMessageVO {
	private String message;
	
	public String toJson(){
		return "{\"message\":\"" + message + "\"}";
	}
	
}
