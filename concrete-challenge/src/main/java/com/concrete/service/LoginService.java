package com.concrete.service;

import com.concrete.entity.User;
import com.concrete.vo.LoginVO;

public interface LoginService {

	User login(LoginVO vo) throws Throwable;
	
} 
