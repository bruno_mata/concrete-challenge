package com.concrete.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.concrete.dao.UserDAO;
import com.concrete.entity.User;
import com.concrete.exception.TokenExpiredException;
import com.concrete.service.SecurityService;
import com.concrete.vo.ErrorMessageVO;

@Component
public class SecurityServiceImpl implements SecurityService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public User checkToken(String token) throws TokenExpiredException {
		
		User user = userDAO.findByToken(token);
		
		if (user == null){
			throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED).entity(new ErrorMessageVO("N�o autorizado")).build());
		}
		
		if (user.getLastLogin().before(Date.from(LocalDateTime.now().plusMinutes(-30).atZone(ZoneId.systemDefault()).toInstant()))){
			throw new TokenExpiredException();
		}
		
		return user;
	}

}
