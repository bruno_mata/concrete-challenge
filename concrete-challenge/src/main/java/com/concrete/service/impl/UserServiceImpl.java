package com.concrete.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.concrete.dao.UserDAO;
import com.concrete.entity.User;
import com.concrete.exception.UserAlreadyExistsException;
import com.concrete.service.UserService;
import com.concrete.util.SecurityUtils;

@Component
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;

	@Override
	@Transactional
	public User createUser(User user) throws Throwable {
		
		try {
		
			user.setPassword(SecurityUtils.hashSHA1(user.getPassword()));
			user.setCreated(new Date());
			user.setModified(new Date());
			user.setToken(SecurityUtils.generateToken(user));
			user.setLastLogin(new Date());
			user.getPhones().stream().forEach(phone -> {phone.setUser(user);});
			return userDAO.create(user);
			
		}catch (Throwable t) {
			if (t.getCause().toString().contains("integrity constraint violation: unique constraint or index violation")){
				throw new UserAlreadyExistsException();
			}
			throw t;
		}
		
	}

}
