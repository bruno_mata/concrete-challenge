package com.concrete.service.impl;

import java.util.Date;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.concrete.dao.UserDAO;
import com.concrete.entity.User;
import com.concrete.service.LoginService;
import com.concrete.util.SecurityUtils;
import com.concrete.vo.ErrorMessageVO;
import com.concrete.vo.LoginVO;

@Component
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public User login(LoginVO vo) throws Throwable {

		User user = userDAO.findByEmailAndPws(vo.getEmail(), SecurityUtils.hashSHA1(vo.getPassword()));
		
		if (user == null) {
			throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED).entity(new ErrorMessageVO("Usu�rio e/ou senha inv�lidos")).build());
		}
		
		user.setToken(SecurityUtils.generateToken(user));
		user.setLastLogin(new Date());
		
		userDAO.update(user);
		
		return user;
	}

}
