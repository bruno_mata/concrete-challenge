package com.concrete.service;

import javax.ws.rs.NotAuthorizedException;

import com.concrete.entity.User;
import com.concrete.exception.TokenExpiredException;

public interface SecurityService {

	User checkToken(String token) throws NotAuthorizedException,TokenExpiredException;
	
}
