package com.concrete.service;

import com.concrete.entity.User;

public interface UserService {

	User createUser(User user) throws Throwable;
	
}
