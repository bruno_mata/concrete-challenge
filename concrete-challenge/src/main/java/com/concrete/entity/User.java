package com.concrete.entity;

import java.io.Serializable;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.security.auth.Subject;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



@Table(name="user")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class User implements Serializable, Principal {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=100)
	private String code;
	
	@Column(nullable=false, length=100)
	private String name;
	
	@Column(unique=true, nullable=false, length=256)
	private String email;
	
	@Column(nullable=false, length=100)
	private String password;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;
	
	@Column(length=100)
	private String token;
	
	@OneToMany(mappedBy="user", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<Phone> phones;
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public boolean implies(Subject subject) {
		return Principal.super.implies(subject);
	}
	
}
