package com.concrete.dao;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.springframework.stereotype.Component;

import com.concrete.entity.User;

@Component
public class UserDAO extends BaseDAO<User> {

	public UserDAO() {
		super(User.class);
	}
	
	public User create(User user) throws Throwable {
		try {
			return persist(user);
		} catch (Throwable t) {
			getSessisionFactory().getCurrentSession().clear();
			throw t;
		}
	}
	
	public User update(User user) throws Throwable {
		try {
			return merge(user);
		} catch (Throwable t) {
			getSessisionFactory().getCurrentSession().clear();
			throw t;
		}
	}
	
	public User findByToken(String token) {
		String sql = "select user from User user left join fetch user.phones where user.token = :token";
		Query query = getSessisionFactory().getCurrentSession().createQuery(sql);
		query.setParameter("token", token);
		try {
			return (User) query.uniqueResult();
		}catch(NonUniqueResultException e) {
			return null;
		}
		
	}
	
	public User findByEmailAndPws(String email, String password) {
		String sql = "select user from User user left join fetch user.phones where user.email = :email and user.password = :password";
		Query query = getSessisionFactory().getCurrentSession().createQuery(sql);
		query.setParameter("email", email);
		query.setParameter("password", password);
		try {
			return (User) query.uniqueResult();
		}catch(NonUniqueResultException e) {
			return null;
		}
	}
	
}
