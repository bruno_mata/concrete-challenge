package com.concrete.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseDAO<T> {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Class<T> persistentClass;
	
	public BaseDAO(Class<T> clazz) {
		persistentClass = clazz;
	}
	
	protected SessionFactory getSessisionFactory(){
		return sessionFactory;
	}
	
	protected T persist(T entity){
		generateCode(entity);
		getSessisionFactory().getCurrentSession().persist(entity);
		return entity;
	}
	
	protected T merge(T entity){
		getSessisionFactory().getCurrentSession().merge(entity);
		return entity;
	}
	

	private void generateCode(T entity){
		
		Optional<Method> opMethod = Arrays.asList(persistentClass.getDeclaredMethods()).stream()
				.filter(method -> method.getName().equals("setCode") && method.getParameterTypes().length == 1 && method.getParameterTypes()[0].equals(String.class))
				.findFirst();
		
		if (opMethod.isPresent()){
			try {
				opMethod.get().invoke(entity, UUID.randomUUID().toString());
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
