package com.concrete.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;
import java.security.MessageDigest;

import com.concrete.entity.User;

public class SecurityUtils {
	
	public static final String SHA_1_HASH_ALGORITHM = "SHA-1";

	public static String hashSHA1(String toHash) {
		
		try {
			
			// Create MessageDigest instance for SHA-1
			MessageDigest md = MessageDigest.getInstance(SHA_1_HASH_ALGORITHM);
			// Get the hash's bytes
			byte[] bytes = md.digest(toHash.getBytes());
			// This bytes[] has bytes in decimal format;
			// Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			// Get complete hashed password in hex format
			return sb.toString();
		
		}catch (Throwable e) {
			e.printStackTrace();
		}
		
		return toHash;
		
	}

	public static String generateToken(User u) {
		
		Key key = MacProvider.generateKey();
		
        Claims claims = Jwts.claims().setSubject(u.getEmail());
        claims.put("lastLogin", u.getLastLogin() + "");

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();
		
    }
	
}
