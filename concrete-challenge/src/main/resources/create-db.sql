CREATE TABLE user (
  id BIGINT IDENTITY PRIMARY KEY NOT NULL,
  code VARCHAR(100) not null,
  name VARCHAR(100) not null,
  email  VARCHAR(100) unique not null,
  password VARCHAR(100) not null,
  created TIMESTAMP not null,
  modified TIMESTAMP not null,
  lastLogin TIMESTAMP not null,
  token VARCHAR(256) not null
);

create table phone (
	ddd INTEGER NOT NULL,
	number INTEGER NOT NULL,
	user_id BIGINT NOT NULL,
	PRIMARY KEY (ddd,number),
	foreign key (user_id) references user(id)
)